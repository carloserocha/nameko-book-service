from nameko.rpc import rpc
from nameko_redis import Redis
from uuid import uuid4

class BookService:
    name = "books_service"
    redis = Redis("development", decode_responses=True, encoding="utf-8")

    @rpc
    def get(self, bookId):
        return self.redis.get(bookId)

    @rpc
    def create(self, book):
        bookId = uuid4().hex
        self.redis.set(bookId, book)
        return bookId