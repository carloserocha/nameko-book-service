from nameko.rpc import RpcProxy
from nameko.web.handlers import http
import json

class GatewayService:
    name = "gateway"

    books_rpc = RpcProxy("books_service")

    @http('GET', '/book/<string:bookId>')
    def get_book(self, request, bookId):
        book = self.books_rpc.get(bookId)
        return json.dumps({'book': book})


    @http('POST', '/book')
    def post_book(self, request):
        data = json.loads(request.get_data(as_text=True))
        bookId = self.books_rpc.create(data['book'])
        return bookId